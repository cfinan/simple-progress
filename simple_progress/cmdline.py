"""
A small script that allows progress monitoring to be passed
 through on the command line
"""
from simple_progress import progress
from datetime import datetime
import re
import argparse
import sys
import os
import pprint as pp


# Move a load of these to constants
# The prefix for verbose messages and the location to write the messages
MSG_PREFIX = '[info]'
MSG_OUT = sys.stderr


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """
    The main entry point for the script
    """
    # Initialise and parse the command line arguments
    parser = init_cmd_args()
    args = parse_cmd_args(parser)

    prog = None
    if args.nlines > 0:
        prog = progress.RemainingProgress(
            nlines=args.nlines,
            units=args.units,
            default_msg=args.msg,
            file=MSG_OUT,
            verbose=not args.quiet
        )
    else:
        prog = progress.RateProgress(
            units=args.units,
            default_msg=args.msg,
            file=MSG_OUT,
            verbose=not args.quiet
        )

    try:
        if args.steps is None or args.steps <= 0:
            init = False

            while True:
                row = sys.stdin.buffer.readline()
                if row == b'':
                    break

                try:
                    prog.next()
                except TypeError:
                    prog.log_start()
                    init = True
                sys.stdout.buffer.write(row)
            if init is True:
                prog.log_finish()

        else:
            start_time = datetime.now()
            row_idx = 1
            total_rows = 0
            while True:
                row = sys.stdin.buffer.readline()
                if row == b'':
                    break
                if row_idx == args.steps:
                    curr_time = datetime.now()
                    time_taken = curr_time - start_time
                    time_taken_str = re.sub(r'\.\d+', '', str(time_taken))
                    total_rows += row_idx

                    try:
                        rows_per_sec = int(total_rows / time_taken.seconds)
                    except ZeroDivisionError:
                        rows_per_sec = total_rows
                    row_idx = 1
                    print(
                        "*-> {3}: ({4} {1}/s) {0} {1}: {2}".format(
                            total_rows,
                            args.units,
                            args.msg,
                            time_taken_str.zfill(9),
                            rows_per_sec
                        ),
                        file=MSG_OUT,
                        flush=True
                    )
                sys.stdout.buffer.write(row)
                row_idx += 1
    except BrokenPipeError:
        sys.exit(0)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def init_cmd_args():
    """
    Initialise the command line arguments and return the parsed arguments

    Returns
    -------
    args : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description="cmd-line progress monitor"
    )

    parser.add_argument(
        '-s', '--steps',
        type=int,
        help="Do not output continuous progress output steps after --steps"
        " rows"
    )
    parser.add_argument('-n', '--nlines',
                        type=int,
                        help="Optional number of lines")
    parser.add_argument('-u', '--units',
                        type=str,
                        default='rows',
                        help="The units of progress (default: rows)")
    parser.add_argument('-m', '--msg',
                        default="processing...",
                        type=str,
                        help="The processing msg")
    parser.add_argument('-q', '--quiet',  action="store_true",
                        help="do not output any progress")
    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def parse_cmd_args(parser):
    """
    Initialise the command line arguments and return the parsed arguments

    Parameters
    ----------
    args : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added

    Returns
    -------
    args : :obj:`argparse.Namespace`
        The argparse namespace object
    """
    args = parser.parse_args()

    if args.nlines is None:
        args.nlines = -1

    return args


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
