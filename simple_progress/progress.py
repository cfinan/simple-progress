"""
Designed to replace the animate module. A lot of things that control
messaging and document script progress
"""

# Used to access stderr
import sys

# Used in the reprinter
import re

# Used to inspect objects
import inspect

# Used to calculate time elapsed in the LineAnimator
import datetime


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Msg(object):
    """
    Outputs messages based on verbosity.
    prefix ~ text put at the start of each message
    outfile ~ option to direct the output elsewhere i.e. sys.stderr
    """
    def __init__(self, verbose=True, prefix="", file=sys.stdout):
        """
        Initialise

        Parameters
        ----------

        verbose : :obj:`bool` (optional)
            The verbosity of the message, if False then any calls
            to msg are not displayed
        prefix : :obj:`str` (optional)
            A prefix to add to every message when msg is called. The
            default is no prefix
        file : :obj:`FileLikeoObject`
            Where the message should be directed. The default is STDOUT
            but can be changed to STDERR by passing sys.stderr. Also can
            be redirected to a file if needed
        """

        # Make sure verbose is boolean
        if not isinstance(verbose, bool):
            raise TypeError('verbose should be true or false')

        self.verbose = verbose
        self.prefix = prefix
        self.file = file

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def msg(self, m, verbose=None, prefix=None, file=None):
        """
        Print the message if verbosity is True

        Parameters
        ----------
        m : :obj:`str`
            The message to print
        prefix : :obj:`str`, optional
            An optional prefix to temporarily overide any global prefix
            set when the object was created. This saves adjusting the
            prefix for small adjustments
        outfile : :obj:`file`, optional
            An optional outfile to temporarily overide any global outfile
            set when the object was created, i.e. sys.stderr
        verbose : :obj:`Bool`, optional
            An optional verbosity to temporarily overide any global
            verbosity set when the object was created
        """

        # If the optional verbose argument has not been set then we set to the
        # verbosity of the object
        if verbose is None:
            verbose = self.verbose

        # If we are being verbose
        if verbose is True:
            # Use local or global prefix and/or outfile
            prefix = prefix or self.prefix
            file = file or self.file

            # If prefix is defined add a single space
            if prefix:
                prefix = prefix + " "

            # Output the message
            print("%s%s" % (prefix, m), file=file)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def msg_atts(self, obj, **kwargs):
        """
        Print the attributes of an object message if verbosity is True

        Parameters
        ----------
        atts : :obj:`str`
            An object who's attributes can be obtained with getattr
        prefix : :obj:`str`, optional
            An optional prefix to temporarily overide any global prefix
            set when the object was created. This saves adjusting the
            prefix for small adjustments
        outfile : :obj:`file`, optional
            An optional outfile to temporarily overide any global outfile
            set when the object was created, i.e. sys.stderr
        verbose : :obj:`Bool`, optional
            An optional verbosity to temporarily overide any global
            verbosity set when the object was created
        prog_name : :obj:`str`, optional
            A program name argument, if passed then a mini program name banner
            will be displayed before outputting the obj attributes
        versiob : :obj:`float`, optional
            The version number for prog_name, only supply if prog_name is given
        """

        try:
            # See if we have the prog_name and version arguments. If so then
            # msg them out and also remove from kwargs so they do not cause an
            # error when passed to msg
            prog_name = kwargs.pop('prog_name')
            version = ''
            if 'version' in kwargs:
                version = " v%s" % str(kwargs.pop('version'))
            self.msg("= %s%s =" % (prog_name, version), **kwargs)
        except KeyError:
            pass

        # Loop through the objects attributes
        for i in inspect.getmembers(obj):
            # Ignores anything starting with underscore
            # (that is, private and protected attributes)
            if not i[0].startswith('_'):
                # Ignores methods
                if not inspect.ismethod(i[1]):
                    attribute = str(i[1])
                    modifier = "value"
                    # For lists, dicts and tuples we report the length
                    if isinstance(i[1], list) is True or\
                       isinstance(i[1], dict) is True or\
                       isinstance(i[1], tuple) is True:
                        attribute = str(len(i[1]))
                        modifier = "length"
                    # Output the message, here we use clean password of lazy
                    # SQLAlchemy to make sure any passwords are stripped
                    # from connection URLS
                    self.msg("%s %s: %s" % (i[0],
                                            modifier,
                                            attribute),
                             **kwargs)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Reprinter(object):
    """
    Displays messages on the terminal and overwrites any previously resplayed
    messages. I got the idea for this from stackoverflow
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, file=sys.stdout):
        '''
        the constructor

        Parameters
        ----------

        file : :obj:`FileLikeObject`
            Where the message should be directed. The default is STDOUT but can
            be changed to STDERR by passing sys.stderr. Also can be redirected
            to a file if needed
        '''

        # Store the arguments to the object
        self.file = file

        # This stores the current text being displayed so that it can be
        # used to clear the terminal for the new text to be displayed
        self.text = ''

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def reprint(self, text):
        '''
        print a line to the terminal and overwrite the previous line

        Parameters
        ----------

        text : :obj:`str`
            The text to be displayed on the terminal
        '''

        # This pads the incomming string with spaces so it is long enough to
        # cover the previous string
        newtext = self.get_pad_string(text)

        # store the text that will be deisplayed so it can be
        # used next time to detmine what needs covering up
        self.text = text

        # Output the padded text
        self.file.write("\r%s" % (newtext))
        self.file.flush()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_pad_string(self, newstring):
        """
        Based on the previous text the reprinter has output. This pads
        newstring with space to coverup previous input

        Parameters
        ----------

        newstring : :obj:`str`
            A string that will be output over the original text
        """

        # Get the length of the current line on the terminal without
        # any padding spaces at the end
        self.text = re.sub(r"\s+$", "", self.text)

        # Generate a padding string that will cover up any overhagging
        # text from the previous output
        pad = " " * max(0, (len(self.text) - len(newstring)))

        return "%s %s" % (newstring, pad)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class BaseAnimation(object):
    """
    A simple animation sequence base class
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, sequence):
        """
        The constructor

        Parameters
        ----------

        sequence : :obj:`list` of `str`
            The sequence that will be animated through

        Raises
        ------

        ValueError
            If the sequence is length 0
        """

        if len(sequence) == 0:
            raise ValueError("animation sequence must have length > 0")

        self.sequence = sequence
        self.seq_len = len(sequence)
        self._inc = -1

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def current_element(self):
        """
        Get the current element in the animation sequence

        Returns
        -------

        current_element : :obj:`str`
            The current element in the animation sequence
        """

        return self.sequence[self._inc]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def next_element(self):
        """
        Get the next element in the animation sequence

        Returns
        -------

        next_element : :obj:`str`
            The next element in the animation sequence
        """

        self._inc += 1

        # get the next in the sequence of the animation character
        self._inc = self._inc % self.seq_len

        # Increment the length of the inctrail
        return self.sequence[self._inc]


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class WindmillAnimation(BaseAnimation):
    """
    The classic turning sails of a windmill
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self):
        """
        The constructor

        Parameters
        ----------

        sequence : :obj:`list` of `str`
            The sequence that will be animated through

        Raises
        ------

        ValueError
            If the sequence is length 0
        """

        sequence = (' | ', ' / ', '---', " \\ ")
        super(WindmillAnimation, self).__init__(sequence)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class PongAnimation(BaseAnimation):
    """
    A ball between two bats
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self):
        """
        The constructor

        Parameters
        ----------

        sequence : :obj:`list` of `str`
            The sequence that will be animated through

        Raises
        ------

        ValueError
            If the sequence is length 0
        """

        sequence = ['|o     |',
                    '| o    |',
                    '|  o   |',
                    '|   o  |',
                    '|    o |',
                    '|     o|']
        sequence = sequence + [i for i in reversed(
            sequence[1:len(sequence)-1])]
        super(PongAnimation, self).__init__(sequence)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class NightRiderAnimation(BaseAnimation):
    """
    A night rider style back and forth
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self):
        """
        The constructor

        Parameters
        ----------

        sequence : :obj:`list` of `str`
            The sequence that will be animated through

        Raises
        ------

        ValueError
            If the sequence is length 0
        """

        sequence = ['[=-     ]',
                    '[-=-    ]',
                    '[ -=-   ]',
                    '[  -=-  ]',
                    '[   -=- ]',
                    '[    -=-]',
                    '[     -=]']
        sequence = sequence + [i for i in reversed(
            sequence[1:len(sequence)-1])]
        super(NightRiderAnimation, self).__init__(sequence)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class CapacitorAnimation(BaseAnimation):
    """
    Some buzzing electricity - not the greatest
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self):
        """
        The constructor

        Parameters
        ----------

        sequence : :obj:`list` of `str`
            The sequence that will be animated through

        Raises
        ------

        ValueError
            If the sequence is length 0
        """

        sequence = ['[/\/\| |/\/\]',
                    '[\/\/| |\/\/]',
                    '[/\/\| |/\/\]',
                    '[\/\/|~|\/\/]']
        super(CapacitorAnimation, self).__init__(sequence)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class StockTickerAnimation(BaseAnimation):
    """
    Animates words as if they weere on a stock ticker
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, phrase, width):
        """
        The constructor

        Parameters
        ----------

        phrase : :obj:`str`
            The phrase to do a stock ticker animation

        width : :obj:`int`
            The width of the stock ticker in characters

        Raises
        ------

        ValueError
            If the width of the ticker is < 3 or if the phrase <= 0
        """

        # This is a mock sequence so the super class does not error out
        sequence = [' '] * len(phrase)
        super(StockTickerAnimation, self).__init__(sequence)

        # width has to be >= 3
        if width < 3:
            raise ValueError("width must be >= 3")

        # Substitute 2 from the width for the borders
        self.width = width - 2

        # We either pad the phrase out to the width or if it is bigger than
        # the width then we add a single space on the end
        self.phrase = phrase + (' ' * max((self.width - len(phrase)), 1))

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def current_element(self):
        """
        Get the current element in the animation sequence

        Returns
        -------

        current_element : :obj:`str`
            The current element in the animation sequence
        """

        # _inc will have the start position in the phrase. The end position
        # is the start position added to either the end of the phrase or the
        # width (whichever is smaller)
        start_str = self.phrase[self._inc:(self._inc + min(len(self.phrase),
                                                           self.width))]

        # The end str represents the wraping of the string on the ticker
        # this is the portion of the string before _inc. Although, if the width
        # is too small to display the end_str, then end_str will end up being
        # ''
        end_str = self.phrase[0:min(self._inc, (self.width - len(start_str)))]

        # Now fill up any excess spaces between start_str and end_str. I don't
        # think this is necessary anymore as we are padding in the constructor
        spaces = ' ' * (self.width - (len(start_str) + len(end_str)))
        return "[%s%s%s]" % (start_str, spaces, end_str)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def next_element(self):
        """
        Get the next element in the animation sequence

        Returns
        -------

        next_element : :obj:`str`
            The next element in the animation sequence
        """

        # Increment
        self._inc += 1

        # get the next in the sequence of the ticker. The modulus keeps _inc
        # from continually increasing
        self._inc = self._inc % len(self.phrase)

        # now that we have incremented return the current element
        return self.current_element()


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SnakeAnimation(BaseAnimation):
    """
    A snake roaming back and forth
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self):
        """
        The constructor

        Parameters
        ----------

        sequence : :obj:`list` of `str`
            The sequence that will be animated through

        Raises
        ------

        ValueError
            If the sequence is length 0
        """

        sequence = [r'_/\/\@         ',
                    r'_/\___@        ',
                    r' ____/\@       ',
                    r'   _/\/\@      ',
                    r'   _/\___@     ',
                    r'    ____/\@    ',
                    r'      _/\/\@   ',
                    r'      _/\___@  ',
                    r'       ____/\@ ',
                    r'         _/\/\@',
                    r'          _/\|@',
                    r'           _||@',
                    r'           |||@',
                    r'           ||@|',
                    r'           |@||',
                    r'           @|||',
                    r'           @||_',
                    r'          @|/\_']

        # The return leg of the snake inverses the starting leg
        rev_sequence = []
        for i in sequence:
            rev_str = ""
            for i in reversed(i):
                if i == '/':
                    i = "\\"
                elif i == "\\":
                    i = '/'
                rev_str += i
            rev_sequence.append(rev_str)
        sequence.extend(rev_sequence)
        for c, i in enumerate(sequence):
            sequence[c] = "|%s|" % i

        super(SnakeAnimation, self).__init__(sequence)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SimpleProgress(object):
    """
    A class that simply displays a progress animation and optionally a user
    message. It give no indication of time elapsed or time remaining
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, animation=WindmillAnimation(), resolution=None,
                 target_time=1, file=sys.stdout, prefix="",
                 start_msg="[start]", finish_msg="[finish]",
                 msg_prefix='', verbose=True, start_line=0, default_msg='',
                 units='lines'):
        """
        The constructor

        Parameters
        ----------

        animation : :obj:`BaseAnimation`
            An animation object that inherits from BaseAnimation
        resolution : :obj:`int`, optional
            Indicates how many loop increments or next() calls must pass
            before an animation action is fired. Can be used to slow down the
            animation speed in fast loops. The default is to use target_time
            based animation action firing
        target_time : :obj:`int`, optional
            The minimum number of seconds between animation action firing
        file : :obj:`FileLikeObject`, optional
            The destination for the animation, defaults to STDOUT but can be
            redirected to STDERR or a file
        prefix : :obj:`str`, optional
            Text that is written before the animation, the default is ""
        start_msg : :obj:`str`, optional
            A message that is written before the log of the start time of the
            progress
        finish_msg : :obj:`str`, optional
            A message that is written before the log of the start time of the
            progress
        msg_prefix : :obj:`str`, optional
            A prefix that can be added to every message passed to the progress
            either the default message or via next() or animate()
        default_msg : str, optional
            A default message that will be displayed if nothing else is being
            displayed (default='')
        verbose : :obj:`bool`, optional
            Do not output anything
        units : str
            Optional units for update meggasges on the classes that support them

        Raises
        ------

        TypeError
            If the animation object does not inherit from BaseAnimation
        """

        # Make sure the animation is the correct type
        if isinstance(animation, BaseAnimation) is False:
            raise TypeError("animation must be derived from BaseAnimation")

        # Store all the parameters
        self.animation = animation
        self.resolution = resolution
        self.target_time = target_time
        self.file = file
        self.start_msg = start_msg
        self.finish_msg = finish_msg
        self.msg_prefix = msg_prefix
        self.VERBOSE = verbose
        self.prefix = prefix
        self.default_msg = default_msg
        self.units = units

        # The reprinter class is responsible for clearing the old animation
        # message and displaying the new one
        self.reprinter = Reprinter(file=self.file)

        # These keep track if the progress has started or finished
        self.START = False
        self.FINISH = False

        # Log the creation time of the object
        self.CREATION_TIME = datetime.datetime.now()

        # For loggin/displaying the start/finish and time taken
        self.START_TIME = None
        self.FINISH_TIME = None
        self.TOTAL_TIME = None

        # N_INC logs how many calls there have been to next(), this may be
        # different from the number of lines (N_LINES) as the user can change
        # those during calls to next()
        self.N_INC = start_line
        self.N_LINES = 0

        # Used to remove potentially troublesome characters from the progress
        # message
        self.NEWLINE_REGEXP = re.compile(r'[\r\n]')

        # ACT_CALL is the time of the call that resulted in a display change
        # when called via next(). This is used in part to determine if other
        # calls to next() will result in animation actions
        self.LAST_ACT_CALL = None
        self.CURR_ACT_CALL = None

        # INC_CALL is the time of a call to next() irrespective of if it
        # resulted in an animation action
        self.LAST_INC_CALL = None
        self.CURR_INC_CALL = None

        # ANY_CALL is the time if a call to either next() or animate()
        self.CURR_ANY_CALL = None
        self.LAST_ANY_CALL = None

        # Log sif the previous call to next resulted in a display change
        self.PREV_INC = False

        # self.CALL_DELTA = None
        # self.INC_CALL_DELTA = None
        # Here we determine what will cause a progress event within a loop
        # or a next call. This is based on either resolution or time.
        # resolution=N requires N calls to next() (either via progress or
        # direct calls) it initiate a progress event. target_time requires at
        # least target_time to pass between calls to cause an animation event.
        # these prevent very fast loops from causing too many progress calls
        # The rate of progress display change can be controlled in two ways.
        # 1. either by the time since the previous change (_progress_on_time)
        # 2. or by the number of calls to next() (_progress_on_resolution)
        # The progress on time is the default. If resolution is not None
        # then it will be used instead
        self._progress_call = self._progress_on_time
        try:
            self.resolution = int(self.resolution)
            self._progress_call = self._progress_on_resolution
        except TypeError:
            # Called is resolution is None
            pass

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def progress(self, obj, lines=1, start=True, finish=True):
        """
        This is called within the loop, so the user does not have to call
        next() manually withon each loop (unless they want to)

        Parameters
        ----------

        obj : :obj:`Iterable`
            Something that can be iterated over
        lines : :obj:`int`
            The number of line increments in each iteration (defaults to 1)
        default_msg : :obj:`str`, optional
            A default message that is displayed after each loop
        start : :obj:`bool`
            Display the progress start time
        finish : :obj:`bool`
            Display the progress finish time and runtime
        """

        # If we are displaying start times
        if start is True:
            self.log_start()

        # and iterate over the iterable calling next as we go
        for i in obj:
            self.next(lines=lines)
            yield i

        # If we want to display the finsih time and the runtime at the end of
        # the iteration
        if finish is True:
            self.log_finish()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def log_start(self):
        """
        This logs the start of the progress to the terminal and displays any
        additional message along with it

        Raises
        ------

        RuntimeError
            If called twice
        """

        # If we have already started then error out
        if self.START is True:
            raise RuntimeError("start already called")
        else:
            # Other wise indicate that we have started and log the start time
            self.START = True
            self.START_TIME = datetime.datetime.now()

        # This obeys the verbosity and file placement
        if self.VERBOSE is True:
            print("%s %s" % (self.start_msg,
                             str(self._chop_microseconds(self.START_TIME))),
                  file=self.file)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def log_finish(self):
        '''
        This logs the finish of the progress to the terminal

        Raises
        ------

        RuntimeError
            If called before log_start()
        '''

        # If we have not started yet then it is an error
        if self.START is False:
            raise RuntimeError("not started yet")

        # This logs the finshed time sets finished to Trur and calcualtes the
        # time taken
        self.FINISH_TIME = datetime.datetime.now()
        self.FINISHED = True
        self.TOTAL_TIME = self.FINISH_TIME - self.START_TIME

        # This obeys the verbosity and file placement
        if self.VERBOSE is True:
            # Generate a string that will cover previous output
            message = self.reprinter.get_pad_string(
                "\r%s %s [runtime] %s" %
                (self.finish_msg,
                 str(self._chop_microseconds(self.FINISH_TIME)),
                 str(self._chop_microseconds(self.TOTAL_TIME))))

            print(message, file=self.file)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def static_msg(self, message, verbose=None, file=None, prefix=None):
        """
        This outputs a static (non-animation) message according to verbosity.
        This is useful for printing when the animation is running as it will
        print "above" the animation line.

        Parameters
        ----------

        message : :obj:`str`
            A string message out output
        verbose : :obj:`bool`
            Temporarily change the verbosity of the animation object from the
            object's default
        file : :obj:`file like object`
            An alternative output destination to the default in the animation
            object, i.e. sys.stderr
        prefix : :obj:`str`
            An alternative prefix to the one in the animation object
        """

        # Use the passed verbose unless it is None then use the objects
        # verbose
        verbose = verbose or self.VERBOSE

        # Only output the message if the verbosity allows for it
        if verbose is True:
            # Use the local prefix if defined
            prefix = prefix or self.prefix

            # Add a space to the prefix if required
            if prefix.endswith(" ") is False:
                prefix = prefix + " "

            # Use the local outfile if required
            file = file or self.file
            file.flush()

            # Get a string long enough to cover the previous input
            message = self.reprinter.get_pad_string(str(message))

            # Now output
            # print("\r%s%s" % (prefix, m), file=outfile)
            file.write("\r%s%s\n" % (prefix, message))
            file.flush()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def next(self, message=None, lines=1):
        """
        Called ding the next iteration of the loop or can be called as and when
        the user wants to. This increments the progress and the number of
        progressed lines (if different).

        Parameters
        ----------

        message : :obj:`str`
            A message to display along side progress
        lines : :obj:`int`
            Incement the number of lines by something different from 1
        """
        # Increment the number of calls to next N_INC and the number of
        # processed lines N_LINES
        self.N_INC += 1
        self.N_LINES += lines

        # Call any registers the time of any call to the progress object
        # Call inc registers calls times to this next() method
        self._call_any()
        self._call_inc()

        # The animate_call will be the method that decodes if the progress will
        # be updated. If not then the call to next will not result in a
        # change in output
        if self._progress_call() is True:
            self.animation.next_element()
            # If we want to update the progress then animate the progress
            self._msg(message or self.default_msg)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def animate(self, message="", force=False):
        """
        This updates the progress animation but does not change the progress
        increment or the number of lines. The animation is only updated if the
        PREV_INC is True or force is True otherwise nothing will happen.

        Parameters
        ----------

        message : :obj:`str`
            A message to display along side progress
        force : :obj:`bool`
            Will force the animation change even if PREV_INC is False
        """

        if force is True or self.PREV_INC is True:
            # Log the time of the call
            self._call_any()
            self.animation.next_element()
            self._msg(message or self.default_msg)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def msg(self, message=None, force=False):
        """
        This updates the progress message but does change the progress
        animation or the progress increment or the number of lines.
        The message is only updated if the PREV_INC variable is True or
        force is True otherwise nothing will happen. Calls to msg are not
        logged

        Parameters
        ----------

        message : :obj:`str`
            A message to display along side progress
        force : :obj:`bool`
            Will force the animation change even if PREV_INC is False
        """

        if force is True or self.PREV_INC is True:
            self._msg(message or self.default_msg)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_info(self):
        """
        The info portion of the progress message displays useful progress
        information to the user. For simple progress there is no info so it
        returns a space for formatting purposes

        Returns
        -------

        info : :obj:`str`
            For SimpleProgress this is a single space character
        """
        return " "

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def newline(self):
        """
        Print a newline character this is helpfull if things wnt to be written
        after an animation line has been printed
        """

        # We do this according to verbosity
        if self.VERBOSE is True:
            print("", file=self.file)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _msg(self, message):
        """
        Actually controls the output of the progress message

        Parameters
        ----------

        message : :obj:`str`
            A message to display along side progress
        """
        # Only do message generation if verbose is True
        if self.VERBOSE is False:
            return

        info = self.get_info()

        message = "%s%s%s%s" % (self.msg_prefix,
                                self.animation.current_element(),
                                info, message)

        # Remove anu newlines or carage returns from the message that may
        # ruin the animation
        message = self.NEWLINE_REGEXP.sub('', message)

        # Store the current message
        self.cur_message = message

        # Finally output the message
        self.reprinter.reprint(message)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _chop_microseconds(self, delta):
        """
        Remove the microseconds from datetime

        Parameters
        ----------

        delta : :obj:`Date` or `Datetime`
            An object  to remove microseconds from

        Returns
        -------

        whole_time : :obj:`Date` or `Datetime`
            With the microseconds removed
        """

        # The call is different depending on datetime or date
        try:
            ms = delta.microsecond
        except AttributeError:
            ms = delta.microseconds
        # if delta.__class__.__name__ == "datetime":
        #     ms = delta.microsecond
        # else:
        #     ms = delta.microseconds

        return delta - datetime.timedelta(microseconds=ms)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _progress_on_resolution(self):
        """
        The will cause the progress to update if resolution calls have been
        made since the last progress update. This also updates the PREV_INC
        variable

        Returns
        -------

        update : :obj:`bool`
            True if the progress should update False if not
        """

        # If the modulus is 0 then it is time to update
        if self.INC % self.resolution == 0:
            # Log the fact that the previous call reuulted in an update
            self.PREV_INC = True
            return self.PREV_INC

        # If we get here then we do not want to update so log the fact that the
        # call resulted in no update
        self.PREV_INC = False
        return self.PREV_INC

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _progress_on_time(self):
        """
        The will cause the progress to update if the time between the previous
        update and this call is greater than the `target_time`. This also
        updates the PREV_INC variable

        Returns
        -------

        update : :obj:`bool`
            True if the progress should update False if not
        """

        try:
            if (self.CURR_INC_CALL - self.CURR_ACT_CALL).\
               total_seconds() >= self.target_time:
                self._call_action()
                # Log the fact that the previous call reuulted in an update
                self.PREV_INC = True
                return self.PREV_INC
        except TypeError:
            self._call_action()
            # Log the fact that the previous call reuulted in an update
            self.PREV_INC = True
            return self.PREV_INC

        # If we get here then we do not want to update so log the fact that the
        # call resulted in no update
        self.PREV_INC = False
        return self.PREV_INC

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _call_any(self):
        """
        Called after next() or animate is called and logs the time of the
        call and stores the time of the previous call. Note this is not called
        in response to msg() being called
        """
        self.LAST_ANY_CALL = self.CURR_ANY_CALL
        self.CURR_ANY_CALL = datetime.datetime.now()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _call_inc(self):
        """
        Called after next() being called. and logs the time of the
        call and stores the time of the previous call. Note this is not called
        in response to animate() or msg() being called
        """
        self.LAST_INC_CALL = self.CURR_INC_CALL
        self.CURR_INC_CALL = datetime.datetime.now()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _call_action(self):
        """
        Called after next() being called AND a progress event happens. This
        logs the time of the call and stores the time of the previous call.
        Note this is not called in response to animate() or msg() being called
        or a call to next() that does not result in a progress update
        """
        self.LAST_ACT_CALL = self.CURR_ACT_CALL
        self.CURR_ACT_CALL = datetime.datetime.now()


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RateProgress(SimpleProgress):
    """
    Used when the user does not know how much they have to progress but it will
    give the amount progress and the rate of that progress
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, *args, **kwargs):
        """
        The constructor

        Parameters
        ----------

        animation : :obj:`BaseAnimation`
            An animation object that inherits from BaseAnimation
        resolution : :obj:`int`, optional
            Indicates how many loop increments or next() calls must pass
            before an animation action is fired. Can be used to slow down the
            animation speed in fast loops. The default is to use target_time
            based animation action firing
        target_time : :obj:`int`, optional
            The minimum number of seconds between animation action firing
        file : :obj:`FileLikeObject`, optional
            The destination for the animation, defaults to STDOUT but can be
            redirected to STDERR or a file
        prefix : :obj:`str`, optional
            Text that is written before the animation, the default is ""
        start_msg : :obj:`str`, optional
            A message that is written before the log of the start time of the
            progress
        finish_msg : :obj:`str`, optional
            A message that is written before the log of the start time of the
            progress
        msg_prefix : :obj:`str`, optional
            A prefix that can be added to every message passed to the progress
            either the default message or via next() or animate()
        verbose : :obj:`bool`, optional
            Do not output anything

        Raises
        ------

        TypeError
            If the animation object does not inherit from BaseAnimation
        """

        super(RateProgress, self).__init__(*args, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_info(self):
        """
        The info portion of the progress message displays useful progress
        information to the user.

        Returns
        -------

        info : :obj:`str`
            For RateProgress this returns the total time taken for all
            processed lines and average time per line
        """

        time_taken = self._chop_microseconds(
            self.CURR_INC_CALL - self.START_TIME)
        total_seconds = float(time_taken.total_seconds())
        try:
            per_sec = float(self.N_LINES) / total_seconds
        except ZeroDivisionError:
            per_sec = 0
        # per_line = (total_seconds) / float(self.N_LINES)
        return " [%s/%i %s|%.2f %s/sec] -> " % (str(time_taken),
                                                self.N_LINES,
                                                self.units,
                                                per_sec,
                                                self.units)


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class RemainingProgress(SimpleProgress):
    """
    Used when the user does not know how much they have to progress but it will
    give the amount progress and the rate of that progress
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, *args, **kwargs):
        """
        The constructor

        Parameters
        ----------

        total_lines : :obj:`int`
            The total number of lines that need processing
        animation : :obj:`BaseAnimation`
            An animation object that inherits from BaseAnimation
        resolution : :obj:`int`, optional
            Indicates how many loop increments or next() calls must pass
            before an animation action is fired. Can be used to slow down the
            animation speed in fast loops. The default is to use target_time
            based animation action firing
        target_time : :obj:`int`, optional
            The minimum number of seconds between animation action firing
        file : :obj:`FileLikeObject`, optional
            The destination for the animation, defaults to STDOUT but can be
            redirected to STDERR or a file
        prefix : :obj:`str`, optional
            Text that is written before the animation, the default is ""
        start_msg : :obj:`str`, optional
            A message that is written before the log of the start time of the
            progress
        finish_msg : :obj:`str`, optional
            A message that is written before the log of the start time of the
            progress
        msg_prefix : :obj:`str`, optional
            A prefix that can be added to every message passed to the progress
            either the default message or via next() or animate()
        verbose : :obj:`bool`, optional
            Do not output anything

        Raises
        ------

        TypeError
            If the animation object does not inherit from BaseAnimation
        ValueError
            if total lines is not > 0
        """

        args = list(args)

        # nlines should be the first positional args to remaining progress
        self.TOTAL_LINES = float(int(args.pop(0)))
        if self.TOTAL_LINES <= 0:
            raise ValueError("nlines needs to be > 0")

        super(RemainingProgress, self).__init__(*args, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_info(self):
        """
        The info portion of the progress message displays useful progress
        information to the user.

        Returns
        -------

        info : :obj:`str`
            For RemainingProgress this returns the processed and remaining
            lines, the % complete and the total time taken and estimated
            remaining time
        """
        # print(self.CURR_INC_CALL)
        # print(self.START_TIME)

        try:
            time_taken = self._chop_microseconds(
                self.CURR_INC_CALL - self.START_TIME)
        except TypeError as e:
            raise TypeError("the start has not been logged!") from e
    # per_line = (float(time_taken.total_seconds()) / float(self.N_LINES))

        perc_complete = min((self.N_LINES/self.TOTAL_LINES)
                            * float(100), 100.0)
        perc_remain = float(100) - perc_complete

        try:
            rem_time = self._chop_microseconds(
                datetime.timedelta(
                    seconds=(time_taken.total_seconds() *
                             (perc_remain/perc_complete))))
        except ZeroDivisionError:
            rem_time = "--:--:--"

        return " [%i/%i][%0.2f%%][%s<%s] -> " % (self.N_LINES,
                                                 self.TOTAL_LINES,
                                                 perc_complete,
                                                 time_taken,
                                                 rem_time)
