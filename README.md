# Simple progress monitors for python
These are not progress bars but animations that can be animated with a call to animate or by wrapping in a loop.

## Installation
Install with:

For development, in the root of the project directory:
```
pip install -e .
```

Non-development, in the root of the project directory:
```
pip install .
```
